/**
 * View Models used by Spring MVC REST controllers.
 */
package com.yuceef.formation.web.rest.vm;
